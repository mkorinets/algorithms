const _ = require('lodash');


const n = 300;
const input = _.shuffle([...Array(n).keys()]); // create unsorted array with n elements
// const input = [ 10, 4, 8, 7, 6, 2, 3 ];


console.log('In: ', input);
console.time('run time');


function sort(orig) {

    // copy original for debugging
    const A = [...orig];

    // create two halves
    let C = A.splice(0, Math.floor(A.length / 2));
    let D = A;

    // recursively sort each, directly return base case
    C = C.length < 2 ? C : sort(C);
    D = D.length < 2 ? D : sort(D);

    // merge
    let i = 0;
    let j = 0;
    const B = Array.from({length: C.length + D.length}); // create placeholder result array with sum length


    B.forEach((v, k) => {

        // whatever array has the smallest number for current position
        if ((C[i] < D[j]) || (D[j] === undefined)) { // or if D is drained
            B[k] = C[i];       // put it into resulting array
            i++;               // then move to next position
        } else {
            B[k] = D[j];       // put it into resulting array
            j++;               // then move to next position
        }

    });

    return B;
}

const output = sort(input);

console.log('Out: ', output);
console.timeEnd('run time');
