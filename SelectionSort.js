const _ = require('lodash');


const n = 300;
const input = _.shuffle([...Array(n).keys()]); // create unsorted array with n elements
const output = [];

console.log('In: ', input);
console.time('a');

for (let i = 0; i <= input.length - 1; i++) {
    let m = null; // min value
    let mI = null; // min value index

    // lookup next min value in the (rest of the) original array
    input.forEach((v, i) => {
        if (m === null || v < m) {
            // first run or current less then min, write it as min
            m = v;
            mI = i; // copy element's index
        }
    });

    output.push(m); // push found value to output array
    delete input[mI]; // delete pushed value from original array
}

console.log('Out: ', output);
console.timeEnd('a');
